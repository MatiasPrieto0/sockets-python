import socket

ClientSocket = socket.socket()
host = input("ingresar host(localhost): ")
port = int(input("ingresar puerto(8050): "))

print('Waiting for connection')
try:
    ClientSocket.connect((host, port))
except socket.error as e:
    print(str(e))

Response = ClientSocket.recv(1024)
while True:
    Input = input('Say Something: ')
    ClientSocket.send(str.encode(Input))
    Response = ClientSocket.recv(1024)
    print(Response.decode('utf-8'))
    if(Input=="close"):
        break

ClientSocket.close()
